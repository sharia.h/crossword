import random

class Crossword:
    def __init__(self, size):
        self.size = size
        self.grid = [[' ' for _ in range(size)] for _ in range(size)]
        self.words = []

    def add_word(self, word):
        if len(word) > self.size:
            return False
        orientation = random.choice(['horizontal', 'vertical', 'diagonal'])
        if orientation == 'horizontal':
            row, col = self.get_start_horizontal(word)
            if row == -1:
                return False
            for i, letter in enumerate(word):
                if self.grid[row][col+i] != ' ' and self.grid[row][col+i] != letter:
                    return False
            for i, letter in enumerate(word):
                self.grid[row][col+i] = letter
            self.words.append(word)
            return True
        elif orientation == 'vertical':
            row, col = self.get_start_vertical(word)
            if row == -1:
                return False
            for i, letter in enumerate(word):
                if self.grid[row+i][col] != ' ' and self.grid[row+i][col] != letter:
                    return False
            for i, letter in enumerate(word):
                self.grid[row+i][col] = letter
            self.words.append(word)
            return True
        else:
            row, col = self.get_start_diagonal(word)
            if row == -1:
                return False
            for i, letter in enumerate(word):
                if self.grid[row+i][col+i] != ' ' and self.grid[row+i][col+i] != letter:
                    return False
            for i, letter in enumerate(word):
                self.grid[row+i][col+i] = letter
            self.words.append(word)
            return True

    def get_start_horizontal(self, word):
        for i in range(self.size):
            for j in range(self.size):
                if self.grid[i][j] == ' ':
                    if j + len(word) <= self.size:
                        if all([self.grid[i][j+k] == ' ' or self.grid[i][j+k] == word[k] for k in range(len(word))]):
                            return i, j
        return -1, -1

    def get_start_vertical(self, word):
        for i in range(self.size):
            for j in range(self.size):
                if self.grid[i][j] == ' ':
                    if i + len(word) <= self.size:
                        if all([self.grid[i+k][j] == ' ' or self.grid[i+k][j] == word[k] for k in range(len(word))]):
                            return i, j
        return -1, -1

    def get_start_diagonal(self, word):
        for i in range(self.size):
            for j in range(self.size):
                if self.grid[i][j] == ' ':
                    if i + len(word) <= self.size and j + len(word) <= self.size:
                        if all([self.grid[i+k][j+k] == ' ' or self.grid[i+k][j+k] == word[k] for k in range(len(word))]):
                            return i, j
        return -1, -1

    def fill_blanks(self):
        for i in range(self.size):
            for j in range(self.size):
                if self.grid[i][j] == ' ':
                    self.grid[i][j] = random.choice('ABCDEFGHIJKLMNOPQRSTUVWXYZ')

    def display(self):
        for row in self.grid:
            print(' '.join(row))

if __name__ == '__main__':
    size=int(input())
    words = list(input().split(","))
    crossword = Crossword(size)
    for word in words:
        while not crossword.add_word(word):
            pass
    crossword.fill_blanks()
    crossword.display()